增值回饋分享金查詢 - getShareBonusRefund
===

## 描述:

判斷保戶本地端db是否有資料，有則回傳沒有則呼叫核心並寫進本地端db

## Request

| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 測試環境 API 地址 |/policy/getShareBonusRefund|
| 正式環境 API 地址 |  |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | N/A |

### Body

```json=
{
   "uuid":"f17e6096-8280-42c6-aea9-8c24e88fbcce",
   "policyCode":"xx1"
}
```

### Body 說明

M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| policyCode | M | String | 保單號碼 ||
| uuid | M | String | 會員流水號 ||

## Response

### 成功

```json=
{
    "status": "A0001",
    "success": true,
    "message": "success",
    "data": {
        "detail": [
            {
                "policyCode": "xx1",
                "anniversaryDate": "2021/12/16",
                "policyYear": 1,
                "shareBonusAmount": "66.10",
                "shareBonusChoiceDesc": "儲存生息",
                "productName": "XXX 險種",
                "insuredName": "安*",
                "money": "新臺幣",
                "liabilityStatusDesc": "生效"
            },
            {
                "policyCode": "xx1",
                "anniversaryDate": "2021/12/16",
                "policyYear": 1,
                "shareBonusAmount": "30.10",
                "shareBonusChoiceDesc": "購買增額繳清保險",
                "productName": "XXX 險種",
                "insuredName": "安*",
                "money": "新臺幣",
                "liabilityStatusDesc": "生效"
            },
            {
                "policyCode": "xx1",
                "anniversaryDate": "2021/12/16",
                "policyYear": 1,
                "shareBonusAmount": "111,100,000",
                "shareBonusChoiceDesc": "現金給付",
                "productName": "XXX 險種",
                "insuredName": "安*",
                "money": "新臺幣",
                "liabilityStatusDesc": "生效"
            },
            {
                "policyCode": "xx1",
                "anniversaryDate": "2021/12/16",
                "policyYear": 1,
                "shareBonusAmount": "111.96",
                "shareBonusChoiceDesc": "現金給付",
                "productName": "XXX 險種",
                "insuredName": "安*",
                "money": "新臺幣",
                "liabilityStatusDesc": "生效"
            }
        ],
        "calAmount": {
            "savingAmount": "66.10",
            "paidUpInsuranceAmount": "30.10",
            "cashAmount": "111,100,111.96"
        }
    }
}
```

### 失敗

```json=
{
    "status": "500",
    "errCode": "E9999",
    "errMsg": "系統繁忙，請稍後再操作",
}
```

### Response 說明

M 為必要傳送欄位，O 為自由選擇傳送欄位。

#### 成功

| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| status | M | String | 狀態碼。<br>A0001成功|
| success | M | Boolean | true/false |
| message | M | String | 成功/錯誤訊息 |
| data | O | Object | 資料(無資料時回傳null) |

#### 失敗

| 欄位名稱名稱 | 資料型別|必要 |  說明
| --------- | ------- | ------- | --------|
| status | String| M | 錯誤狀態碼(請見錯誤代碼表)|
| errCode | String | M |  錯誤代碼(請見錯誤代碼表)|
| errMsg | String | M |  錯誤訊息(請見錯誤代碼表)|

#### data 欄位

| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| success | M | Integer |完成|
| statusChange | M | Integer |進行中|
| detail | O | Object |詳細資訊(無資料時回傳null)|
| calAmount | O | Object |狀態比數資訊|

#### detail 欄位

| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| policyCode | M | String |保單號碼|
| anniversaryDate | M | String |保單周年日|
| policyYear | M | Integer |保單年度|
| shareBonusAmount | M | String |當期增值分享金|
| shareBonusChoiceDesc | M | String |領取方式|
| productName | M | String |保單名稱|
| insuredName | M | String |被保人名稱|
| money | M | String |保單幣別|
| liabilityStatusDesc | M | String |保單狀態|

#### calAmount 欄位

| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| savingAmount | M | String |儲存生息總額|
| paidUpInsuranceAmount | M | String |購買增額繳清保險總額|
| cashAmount | M | String |現金給付總額|
 ------------------------------- 

