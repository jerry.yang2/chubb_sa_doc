產生驗證圖 - getRandomCode
===
## 描述:
產生四碼隨機數字驗證碼

## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 昕力DEV | http://10.20.30.120:7001 |
| 測試環境 API 地址 | /chubb.apiTemplate/member/getRandomCode |
| 正式環境 API 地址 | N/A |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | N/A |
| Body | N/A |

## Response
```json=
{
  驗證碼圖片
}
```
 ------------------------------- 

