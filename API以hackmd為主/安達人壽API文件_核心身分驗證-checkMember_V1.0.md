核心身分驗證 - checkMember
===
## 描述:  
Step 1. 檢核欄位格式是否符合  
符合 → 則進行第2步  
不符合 → 則回傳[E0005]   
<br>
Step 2. 檢核驗證碼是否符合  
符合 → 則進行第3步   
不符合 → 則回傳[E1009]  
<br>
Step 3. 判斷傳入帳號是否存在(TX_WEBUSER)  
存在 → 則回傳[E2008]    
不存在 → 則進行第4步  
<br>
Step 4. 呼叫核心API撈取保戶資料及保單資料  
存在 → 則回傳該保戶個人資料，並回傳[A0001]   
不存在 → 則回傳[E0007]、[E0008]、[E0009]、[E2006] 

## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 昕力DEV | http://10.20.30.120:7001 |
| 測試環境 API 地址 | /chubb.apiTemplate/member/checkMember?checkCode=1234 |
| 正式環境 API 地址 | N/A |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | checkCode=1234 |
| checkCode | 四碼驗證碼 |

### Body
```json=
{
    "account":"A118324350",
    "birthday":"2021/12/24",
    "cellphone":"0987654321",
    "policyNumber":"ab123cd456ef789",
    "payType":"1"
}
```

### Body 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| account | M | String | 會員帳號 | 身分證或居留證號，不可含特殊符號、長度不能超過10碼並符合身分證或新居留證號規範格式。 | 
| birthday | M | String | 生日 | 只能輸入滿20歲以上的時間(ex:現在2021，只能輸入2001(含)以上)，fmt:[yyyy/mm/dd] |
| cellphone | M | String | 電話號碼 | 輸入限制長度10碼，必須是09開頭，前後不可有空格，不可含特殊符號。 |
| policyNumber | M | String | 保單號碼 | 輸入限制長度15碼，前後不可有空格，不可含特殊符號。 |
| payType | M | String | 繳費方式 | 只能輸入"1","2","3"，<br>1.信用卡<br>2.自行繳費<br>3.轉帳 |

## Response 傳送成功  
```json=
{
    "status": "A0001",
    "success": true,
    "message": "success",
    "data": {
        "email": "abc123@chubb.com",
        "name": "安達",
        "birthday": "2021/12/16",
        "cellphone": "0961000000",
        "account": "A118324350",
        "msg": "將於下一步發送OTP驗證碼至此行動電話與電子信箱",
        "isEmailShare":false,
        "hiddenEmail":"abc***@chubb.com",
        "hiddenCellphone":"0961****00"
    }
}
```
## Response 傳送失敗  
```json=
{
    "status": "500",
    "errCode": "E9999",
    "errMsg": "系統繁忙，請稍後再操作"
}
```

### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| status | M | String | HttpStatus狀態碼 | A0001成功<br>失敗請參考 [錯誤代碼表](/錯誤V1.0.md)|
| errCode | M | String | 後端exception代碼 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| errMsg | M | String | 後端exception代碼訊息資料 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| success | M | Boolean | 是否成功 | true/false |
| message | M | String | 訊息資料 | 成功/錯誤訊息 |
| data | O | Object | 資料(無資料時回傳null) | 成功時才會有 |

#### data 欄位
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| email | M | String | 保戶Email|
| name | M | String | 保戶姓名 |
| birthday | M | String | 保戶生日 | fmt:[yyyy/mm/dd] |
| cellphone | M | String | 保戶電話號碼 |
| account | M | String | 保戶身分證字號|
| msg | M | String | email是否共用訊息 |
| isEmailShare | M | Boolean | email是否共用 | true=共用/false=沒共用 |
| hiddenEmail | M | String | 隱碼email |
| hiddenCellphone | M | String | 隱碼電話 |

 -------------------------------  