變更密碼 - ChangeWhisper
===
## 描述:
Step 1. 檢核新密碼欄位是否符合規則
<br>
  符合 → 則進行第二步
<br>
  不符合 → 則回傳[E0005]
<br>
Step 2. 檢核再次輸入新密碼是否與新密碼符合
<br>
   符合 → 則進行第三步
<br>
  不符合 → 則回傳[E1007]
<br>
Step 3. 驗證OTP
<br>
  符合 → 則進行第四步
<br>
  不符合 → 則回傳[E1011]
<br>
Step 4. 呼叫核心保戶會員密碼變更(登入後變更)
<br>
  成功 → 則進行第五步
<br>
  不成功 → 則回傳核心回傳的錯誤訊息
<br>
Step 5. 寄信


## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 測試環境 API 地址 |/member/changeWisper|
| 正式環境 API 地址 |  |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | N/A |

### Body
```json=
{
    "loginToken":"dsfdsfdsfdsfdsfdsa"
    "oldWhisper": "123456",
    "newWhisper": "Fa123456789",
    "reConfirmNewWhisper": "Fa123456789",
    "otpString": "123456",
    "privateKey": "UQyUkXZYvTEq4TSNFA32sP8KbHQ0Vjy8PAGE0SHI/3o6ZZSJapHjh",
    
}
```

### Body 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| loginToken | M | String | 登入Token | 帳號登入TOKEN |
| whisper | M | String | 舊密碼 | 該保戶舊密碼不做任何檢核 | 
| newWhisper | M | String | 新密碼 | 輸入限制長度8-12位英數字混合字母，必須含至少一位大寫與一位小寫英文與阿拉伯數字。 |
| reConfirmNewWhisper | M | String | 驗證碼驗證要求 |與新密碼一致
| otpString | M | String | OTP驗證碼 |
| privateKey | M | String | 驗證OTP時所需的唯一鍵 |


## Response
```json=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data":null
    
}
```
### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| status | M | String | A0001成功<br>E0001:applicationToken不相符<br>E0005:欄位檢核錯誤<br>E1007:密碼與再輸入密碼不相同<br>E1011:OTP驗證錯誤<br>E9999:系統繁忙，請稍後再操作|
| success | M | Boolean | true/false |
| message | M | String | 成功/錯誤訊息 |
| data | O | Object | 資料(無資料時回傳null) |


#### data 欄位
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |


 ------------------------------- 

