帳號登入 - MemberCathayLogin
===
## 描述:
帳號登入。

Seq_no存在:
　　密碼相同 → 【200】登入成功。
　　密碼不同 → 【50004】密碼錯誤。
　　帳戶鎖定 → 【50099】密碼錯誤達5次，不能再登入。
　　is_set_pw='N' → 【60004】尚未建立密碼。
Seq_no不存在
　　則【60002】找不到資料。

## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 測試環境 API 地址 | http://10.20.30.210:8086/mobileweb/api/member/cathaylogin  |
| 測試環境Gateway| http://10.20.30.209:9527/mobileweb/api/member/cathaylogin |
| 正式環境 API 地址 | https://api.cathay-ins.com.vn:9527/mobileweb/api/member/cathaylogin |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | N/A |

### Body
```json=
{
    "email": "jhuang@mail.com",
    "kor": "bbbb8888"
}
```

### Body 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| email | M | string | 註冊email |
| kor | M | string | 密碼 |

## Response
```json=
{
    "status": 200,
    "message": "sự thành công",
    "success": true,
    "total": 0,
    "data": {
        "seq_no": 1572,
        "customer_name": "CATHERINE",
        "certificate_number_9": "221234569",
        "certificate_number_12": "",
        "birthday": "1997-03-10",
        "email": "jhuang@mail.com",
        "email_bk": "jhuang@mail.com",
        "mobile": "0911111111",
        "sex": "0",
        "county": "002",
        "province": "012",
        "addr": "iiiiii",
        "is_link_id": "N",
        "fb_account": "",
        "fb_title": "",
        "google_account": "",
        "google_title": "",
        "is_link_sns": "N",
        "is_set_pw": "Y",
        "is_validate_policy": "N",
        "new_id": "",
        "remark": "",
        "rgst_list": [
            { "prod_pol_code": "CF02002", 
              "rgst_no": "51F-822.60",
              "create_date": "16/08/2021"},
            { "prod_pol_code": "CF02002", 
              "rgst_no": "51G-048.44",
              "create_date": "16/08/2021"},
            { "prod_pol_code": "CF02002", 
              "rgst_no": "51D-348.99",
              "create_date": "16/08/2021"}
        ]
    },
    "access_token": "eyJhbGciOiJIUzI1NiJ9.eyJjdXN0b21lck5hbWUiOiJDQVRIRVJJTkUiLCJleHAiOjE2MjE2OTIxODQsImlhdCI6MTYyMTY2MzM4NCwiaXNzIjoibW9iaWxlV2ViLWdhdGV3YXkiLCJqdGkiOiIxODRiODU1Zi02ZGNhLTQ4YmItYTU5OS0wZWE1YjdhZDcxOGYiLCJzdWIiOiIxNTcyIn0.fo71ZnGNNZDQzrhx9g1VHlbQAnl_zxlHVUSSwnzp67o"
}
```
### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| status | M | Integer | 成功:200/失敗:521/國泰核心錯誤:999 |
| success | M | Boolean | true/false |
| message | M | String | 成功/錯誤訊息 |
| total | M | Integer | 資料列表總筆數 |
| data | O | Object Array | 資料列表(無資料時回傳null) |
| access_token | M | string | sesseion，登入成功後，Gateway會在回傳JSON加入此欄位 |

#### data 欄位
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| seq_no | M | integer | 會員流水號 |
| customer_name | M | string | 姓名 |
| certificate_number_9 | M | string | 身分證字號9 |
| certificate_number_12 | M | string | 身分證字號12 |
| birthday | M | string | 生日 |
| email | M | string | 註冊email |
| email_bk | M | string | 備用email |
| mobile | M | string | 手機 |
| sex | M | string | 性別，0:女生/1:男生 |
| county | M | string | 省份 |
| province | M | string | 郡縣 |
| addr | M | string | 詳細地址 |
| is_link_id | M | string | 身份證ID連結，N：未綁定，Y：已綁定(審核通過)，F：退回(待補件)，A：申請中，R：已補件(重送) |
| fb_account | M | string | FB帳號 |
| fb_title | M | string | FB名稱 |
| google_account | M | string | Google帳號 |
| google_title | M | string | Google名稱 |
| is_link_sns | M | string | 社群帳號連結，N:未連結/Y:已連結 |
| is_set_pw | M | string | 已設定密碼，N:未設定/Y:已設定 |
| is_validate_policy | M | string | 開通線下保單，N：未開通，Y：已開通 |
| new_id | M | string | is_link_id =N、Y 為空值 |
| remark | M | string | is_link_id =N、Y、A、R時為空值 |
| rgst_list | M | Object Array | 車牌清單List |

#### rgst_list 說明
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| prod_pol_code | M | string | 種類，CF02002:汽車，CF02001:機車 |
| rgst_no | M | string | 車牌號碼 |
| create_date | M | string | 建立日期，fmt:[DD/MM/YYYY] |

#### rgst_list 欄位
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| prod_pol_code | M | String | 汽機車代碼，CF02001=機車、CF02002=汽車|
| rgst_no | M | String | 車牌號碼 |

 ------------------------------- 

