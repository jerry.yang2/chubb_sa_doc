本地身分驗證 - checkLocalMember
===
## 描述:  
Step 1. 檢核欄位格式是否符合  
符合 → 則進行第2步  
不符合 → 則回傳[E0005]   
<br>
Step 2. 判斷傳入帳號是否存在(TX_WEBUSER)  
存在 → 則回傳該保戶個人資料，並回傳[A0001]    
不存在 → 則回傳[E0007]  

## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 昕力DEV | http://10.20.30.120:7001 |
| 測試環境 API 地址 | /chubb.apiTemplate/member/checkLocalMember |
| 正式環境 API 地址 | N/A |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | N/A |  

### Body
```json=
{
    "account":"A118324350",
    "birthday": "1996/12/03",
    "celphone":"0961000000"
}
```

### Body 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| account | M | String | 會員帳號 | 身分證或居留證號，不可含特殊符號、長度不能超過10碼並符合身分證或新居留證號規範格式。 |
| birthday | M | String | 生日 | fmt:[yyyy/mm/dd] |
| cellphone | M | String | 電話號碼 | 輸入限制長度10碼，必須是09開頭，前後不可有空格，不可含特殊符號 |

## Response 傳送成功  
```json=
{
    "status": "A0001",
    "success": true,
    "message": "success",
    "data": {
        "uuid":"asdfasdf-dsadasd-asdvsadf-adsfasdf",
        "hiddenEmail":"abc***@chubb.com",
        "hiddenCellphone":"0961****00",
        "isEmailShare":false
    }
}
```
## Response 傳送失敗  
```json=
{
    "status": "500",
    "errCode": "E9999",
    "errMsg": "系統繁忙，請稍後再操作"
}
```

### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| status | M | String | HttpStatus狀態碼 | A0001成功<br>失敗請參考 [錯誤代碼表](/錯誤V1.0.md)|
| errCode | M | String | 後端exception代碼 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| errMsg | M | String | 後端exception代碼訊息資料 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| success | M | Boolean | 是否成功 | true/false |
| message | M | String | 訊息資料 | 成功/錯誤訊息 |
| data | O | Object | 資料(無資料時回傳null) | 成功時才會有 |

#### data 欄位
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| uuid | M | String | 會員流流水號 |  |
| hiddenEmail | M | String | 隱碼email |
| hiddenCellphone | M | String | 隱碼電話 |
| isEmailShare | M | Boolean | email是否共用 | true=共用/false=沒共用 |

 ------------------------------- 

