取得保戶所有保單 - MemberAllPolicy
===
## 描述:
撈取保戶所有保單


## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 測試環境 API 地址 |/member/getMemberAllPolicy|
| 正式環境 API 地址 |  |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | N/A |

### Body
```json=
   "uuid":"c298db8c-18a3-4cb3-9e21-8a413859890b"
```

### Body 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| uuid | M | String | 會員流水號 ||



## Response
```json=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data":[{
    "policyCode":"xx1", 
    "productName":"XXX 險種", 
    "busiType":1, 
    "busiTypeDesc":"普通", 
    "validateDate":1639650832154, 
    "liabilityStatus":1, 
    "liabilityStatusDesc":"生效",
    "holderName":"安達", 
    "holderCertiCode":"A123456789",
    "insuredName":"安達", 
    "insuredCertiCode":"A123456789",
    "payNext":1, 
    "payNextDesc":"匯款/其他", 
    "chargeMode":5, 
    "chargeModeDesc":"躉繳", 
    "amount":1111, 
    "moneyCode":"TWD",
    "money":"新臺幣",
    "salesCode":"xxxxxxxxxx",
    "agentCode":"Chubb",
    "agentName":"安達人壽", 
    "accountValue":1111
    },{
    "policyCode":"xx2",
    "productName":"XXX 險種",
    "busiType":1, 
    "busiTypeDesc":"普通",
    "validateDate":1639650832154, 
    "liabilityStatus":1, 
    "liabilityStatusDesc":"生效",
    "holderName":"安達", 
    "holderCertiCode":"A123456789", 
    "insuredName":"安達", 
    "insuredCertiCode":"A123456789",
    "payNext":1, 
    "payNextDesc":"匯款/其他", 
    "chargeMode":5, 
    "chargeModeDesc":"躉繳", 
    "amount":1111, 
    "moneyCode":"TWD", 
    "money":"新臺幣", 
    "salesCode":"xxxxxxxxxx", 
    "agentCode":"Chubb", 
    "agentName":"安達人壽", 
    "accountValue":1111
    }]
    
}
```
### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| status | M | String | A0001成功<br>E0001:applicationToken不相符<br>E9999:系統繁忙，請稍後再操作|
| success | M | Boolean | true/false |
| message | M | String | 成功/錯誤訊息 |
| data | O | Object | 資料(無資料時回傳null) |


#### data 欄位
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| policyCode | M | String |保單號碼|
| productName | M | String |商品名稱|
| busiType | M | Integer |資金運作類型代碼|
| busiTypeDesc | M | String |資金運作類型|
| validateDate | M | Long |保單生效日|
| liabilityStatus | M | Integer |保單當前責任狀態碼|
| liabilityStatusDesc | M | String |保單當前責任狀態|
| holderName | M | String |要保人姓名|
| holderCertiCode | M | String |要保人身分證字號|
| insuredName | M | String |被保險人姓名|
| insuredCertiCode | M | String |被保險人身分證字號|
| payNext | M | Integer |繳費方式代碼|
| payNextDesc | M | String |繳費方式|
| chargeMode | M | Integer |繳別代碼|
| chargeModeDesc | M | String |繳別|
| amount | M | Integer |保額|
| moneyCode | M | String |幣別代碼|
| money | M | String |幣別|
| salesCode | M | String |業務員證號|
| agentCode | M | String |通路編號|
| agentName | M | String |通路名稱|
| accountValue | M | Integer |當下帳戶價值|




 ------------------------------- 

