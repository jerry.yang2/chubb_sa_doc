記錄最近使用功能清單 - saveFunctionRecord
===
## 描述:
供前端將用戶的最近使用功能清單儲存入db

## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 測試環境 API 地址 | /member/saveFunctionRecord |
| 正式環境 API 地址 |  |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | N/A |

### Body
```json=
{
    "uuid": "f17e6096-8280-42c6-aea9-8c24e88fbcce",
    "record": "4 5 1"
}
```

### Body 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| record | M | String | 最近使用功能清單 | 儲存形式由前端自行決定，後端只負責儲存且不作任何資料處理|
| uuid | M | String | 會員流水號 ||

## Response
### 成功
```json=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data": null
}
```
### 失敗
```json=
{
    "status": "500",
    "errCode": "E9999",
    "errMsg": "系統繁忙，請稍後再操作",
}
```
### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。
#### 成功
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| status | M | String | 狀態碼。<br>A0001成功|
| success | M | Boolean | true/false |
| message | M | String | 成功/錯誤訊息 |
| data | O | Object | 資料(無資料時回傳null) |

#### 失敗
| 欄位名稱名稱 | 資料型別|必要 |  說明
| --------- | ------- | ------- | --------|
| status | String| M | 錯誤狀態碼(請見錯誤代碼表)|
| errCode | String | M |  錯誤代碼(請見錯誤代碼表)|
| errMsg | String | M |  錯誤訊息(請見錯誤代碼表)|

 ------------------------------- 

