核心首次申請 - firstLogin
===
## 描述:
Step 1. 檢核欄位格式是否符合  
符合 → 則進行第2步  
不符合 → 則回傳[E0005]   
<br>
Step 2. 檢核OTP是否符合  
符合 → 則進行第3步   
不符合第1~2次 → 則回傳[E1012]  
不符合第3次 → 則回傳[E1013]  
<br>
Step 3. 呼叫核心保戶會員新增API  
成功 → 則進行第4步    
失敗 → 則回傳[E9999]  
<br>
Step 4. 將保戶資料存入資料庫(TX_WEBUSER),閱讀聲明存入資料庫(TX_CMSNEW_READING_RECORE)  
成功 → 則進行第5步  
失敗 → 則回傳[E9999]  
<br>
Step 5. 發送首次申請成功通知Email  
成功 → 則回傳[A0001]    
失敗 → 則回傳[E9999]  

## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 昕力DEV | http://10.20.30.120:7001 |
| 測試環境 API 地址 | /chubb.apiTemplate/member/firstLogin |
| 正式環境 API 地址 | N/A |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | N/A |

### Body
```json=
{
    "account":"A123456789",
    "whisper":"xxxxxx",
    "reConfirmWhisper":"xxxxxx", 
    "email": "abc123@chubb.com",
    "name": "安達",
    "birthday": "2021/12/16",
    "cellphone": "0961000000",
    "cmsNewId": {99,399}
}
```

### Body 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。
| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| account | M | String | 會員帳號 | 身分證或居留證號 | 
| whisper | M | string | 密碼 | 輸入限制長度8-12位英數字混合字母，必須含至少一位大寫與一位小寫英文與阿拉伯數字 |
| reConfirmWhisper | M | string | 再次輸入密碼 | 輸入限制長度8-12位英數字混合字母，必須含至少一位大寫與一位小寫英文與阿拉伯數字 |
| email | M | String | 保戶Email |  |  
| name | M | String | 姓名 |  |  
| birthday | M | String | 生日 | fmt:[yyyy/mm/dd] |
| cellphone | M | String | 電話號碼 |  |
| cmsNewId | M | Integer | 閱讀聲明上稿資料流水號 | 04.閱讀聲明-個人資料保護法應告知事項、<br>05.閱讀聲明-網路保險服務聲明事項04和05<br>的上稿ID |

## Response 傳送成功
```json=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data": null
}
```
## Response 傳送失敗
```json=
{
    "status": "500",
    "errCode": "E9999",
    "errMsg": "系統繁忙，請稍後再操作"
}
```
### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| status | M | String | HttpStatus狀態碼 | 成功會是A0001，失敗請參考 [錯誤代碼表](/錯誤V1.0.md)  |
| errCode | M | String | 後端exception代碼 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| errMsg | M | String | 後端exception代碼訊息資料 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| success | M | Boolean | 是否成功 | true/falus |
| message | M | String | 訊息資料 | success |
| data | O | Object | 資料(無資料時回傳null) | 成功時才會有 |

 ------------------------------- 

