取得單一保單詳細資料 - memberSinglePolicy
===
## 描述:
取得指定的保單詳細資料，若無暫存資料則呼叫核心取得資料。

## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 昕力DEV | http://10.20.30.120:7001 |
| 測試環境 API 地址 | /policy/getSinglePolicy |
| 正式環境 API 地址 | N/A |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | N/A |

### Body
```json=
{
    "policyCode": [
        "xx1"
    ],
    "uuid":"asdfasdf-dsadasd-asdvsadf-adsfasdf"
}
```

### Body 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。
| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| policyCode | M | Object Array | 保單號碼 | 可以一次給多筆 |
| uuid | O | String | 會員流流水號 |  |

## Response 傳送成功-登入需知、資訊公告
```json=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data": [
        {
            "policyCode": "xx1",
            "productName": "XXX 險種",
            "busiType": 1,
            "busiTypeDesc": "普通",
            "validateDate": "2021/12/16",
            "liabilityStatus": 1,
            "liabilityStatusDesc": "生效",
            "holderName": "安*",
            "holderCertiCode": "******6789",
            "insuredName": "安*",
            "insuredCertiCode": "******6789",
            "payNext": 1,
            "payNextDesc": "匯款/其他",
            "chargeMode": 5,
            "chargeModeDesc": "躉繳",
            "amount": "1,111",
            "moneyCode": "TWD",
            "money": "新臺幣",
            "salesCode": "xxxxxxxxxx",
            "agentCode": "Chubb",
            "agentName": "安達人壽",
            "accountValue": "1,111",
            "salesName": "X*X",
            "endDate": "2021/12/16",
            "pauseDate": "2021/12/16",
            "productKind": "甲型",
            "chargeYear": 7,
            "coverageYear": 5,
            "premStatus": 1,
            "premStatusDesc": "正常繳費",
            "amountFloatType": 1,
            "amountFloatDesc": "平準型",
            "addressFee": "台北市士林區*****",
            "realAddress": "台北市士林區*****",
            "linkTel": "0900****00",
            "noLapseGuaranntee": "Y",
            "isTday": "Y",
            "paymentStartDate": "2021/12/16",
            "liaChargeMode": 5,
            "liaChargeModeDesc": "躉繳",
            "liaPayMode": 2,
            "liaPayModeDesc": "支票"
        }
    ]
}


```
## Response 傳送失敗
```json=
{
    "status": "500",
    "errCode": "E9999",
    "errMsg": "系統繁忙，請稍後再操作"
}
```
### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| status | M | String | HttpStatus狀態碼 | 成功會是A0001，失敗請參考 [錯誤代碼表](/錯誤V1.0.md)  |
| errCode | M | String | 後端exception代碼 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| errMsg | M | String | 後端exception代碼訊息資料 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| success | M | Boolean | 是否成功 | true/falus |
| message | M | String | 訊息資料 | success |
| data | O | Object | 資料(無資料時回傳null) | 成功時才會有 |

#### data 欄位
| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| policyCode | M | String | 保單號碼 |  |
| productName | M | String | 保單名稱 |  |
| busiType | M | Integer | 資金運作類型代碼 |  |
| busiTypeDesc | M | String | 資金運作類型 |  |
| validateDate | M | String | 保單生效日 | fmt:[yyyy/mm/dd] |
| liabilityStatusDesc | M | String | 保單當前責任狀態 |  |
| liabilityStatus | M | Integer | 保單當前責任狀態代碼 |  |
| holderName | M | String | 要保人姓名 |  |
| holderCertiCode | M | String | 要保人身份證字號 |  |
| insuredName | M | String | 被保險人姓名 |  |
| insuredCertiCode | M | String | 被保險人身份證字號 |  |
| payNext | M | Integer | 繳費方式代碼 |  |
| payNextDesc | M | String | 繳費方式 |  |
| chargeMode | M | Integer | 繳別代碼 |  |
| chargeModeDesc | M | String | 繳別 |  |
| amount | M | String | 保額 |  |
| moneyCode | M | String | 幣別代碼 |  |
| salesCode | M | String | 業務員證號 |  |
| agentCode | M | String | 通路編號 |  |
| agentName | M | String | 通路名稱 |  |
| accountValue | M | String | 當下帳戶價值 |  |
| salesName | M | String | 業務員名稱 |  |
| endDate | M | String | 保單終止日 | fmt:[yyyy/mm/dd] |
| pauseDate | M | String | 保單停效日 | fmt:[yyyy/mm/dd] |
| productKind | M | String | 保單型別 |  |
| chargeYear | M | Integer | 繳費期間 |  |
| coverageYear | M | Integer | 保障期間 |  |
| premStatus | M | Integer | 保費繳費狀態代碼 |  |
| premStatusDesc | M | String | 保費繳費狀態 |  |
| amountFloatType | M | Integer | 身故保險金給付代碼 |  |
| amountFloatDesc | M | String | 身故保險金給付 |  |
| addressFee | M | String | 收費地址 |  |
| realAddress | M | String | 戶籍地址 |  |
| linkTel | M | String | 連絡電話 |  |
| noLapseGuaranntee | M | String | 不停效保證 |  |
| isTday | M | String | T 日資格 |  |
| paymentStartDate | M | String | 年金給付期間開始時間 | fmt:[yyyy/mm/dd] |
| liaChargeMode | M | Integer | 年金給付頻率代碼 |  |
| liaChargeModeDesc | M | String | 年金給付頻率 |  |
| liaPayMode | M | Integer | 年金支付方式代碼 |  |
| liaPayModeDesc | M | String | 年金支付方式 |  |

 ------------------------------- 

