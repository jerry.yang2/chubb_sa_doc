發送otp - sendOTP
===
## 描述:  
Step 1. 判斷UUID是否傳入值  
有值 → 則找出相關資料(會員帳號+手機號碼+電子信箱)，進行第2步   
無值 → 則使用傳入值(會員帳號+手機號碼+電子信箱)，進行第2步   
<br>
Step 2. 判斷是否超過OTP每日上限10次，依據會員帳號計算  
超過 → 則回傳[E5001]  
未超過 → 則進行第3步  
<br>
Step 3. 呼叫核心API取得OTP及發送OTP  
成功 → 則回傳[A0001]     
失敗 → 則回傳[E5002]  

備註:  
若errCode為E5002，則回傳的errMsg為"由於系統中斷，OTP發送失敗，請您選擇繼續申請或取消本次申請，謝謝。@@@由於系統中斷，稍後將回到首頁，需請您重新操作，造成您的不便，敬請見諒！"  
前端記錄otp發送失敗次數  
若失敗次數為1次，則錯誤訊息為errMsg的"@@@"前面的字串   
若失敗次數為>1次，則錯誤訊息為errMsg的"@@@"後面的字串  

## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 昕力DEV | http://10.20.30.120:7001 |
| 測試環境 API 地址 | /chubb.apiTemplate/member/sendOTP |
| 正式環境 API 地址 | N/A |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | N/A |

### Body - 首次申請
```json=
{
    "account": "A123456789",
    "cellphone": "0961000000",
    "email": "XXX@chubb.com",
    "uuid":""
}
```
### Body - 忘記密碼+變更密碼
```json=
{
    "account": "",
    "cellphone": "",
    "email": "",
    "uuid":"asdfasdf-dsadasd-asdvsadf-adsfasdf"
}
```
### Body 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| account | O | String | 會員帳號 | 身分證或居留證號 | 
| cellphone | O | String | 電話號碼 |  |
| email | O | String | 電子信箱 |  |
| uuid | O | String | 會員流水號 | 使用uuid找出相關資料(會員帳號+手機號碼+電子信箱) |

## Response 傳送成功  
```json=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data": {
        "privateKey": "UQyUkXZYvTEq4TSNFA32sP8KbHQ0Vjy8PAGE0SHI/3o6ZZSJapHjh",
        "otpString"： "737913",
        "identityCode"： "AA",
        "expiredSecond"： 300,
        "expiredTime"： "2022/01/10 17:22:43",
        "returnMessage": "OTP驗證碼已寄到您的手機號碼與電子信箱"
    }
}
```
## Response 傳送失敗  
```json=
{
    "status": "500",
    "errCode": "E9999",
    "errMsg": "系統繁忙，請稍後再操作"
}
```
## Response 欄位
M 為必要傳送欄位，O 為自由選擇傳送欄位。
| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| status | M | String | HttpStatus狀態碼 | A0001成功<br>失敗請參考 [錯誤代碼表](/錯誤V1.0.md)|
| errCode | M | String | 後端exception代碼 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| errMsg | M | String | 後端exception代碼訊息資料 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| success | M | Boolean | 是否成功 | true/false |
| message | M | String | 訊息資料 | 成功/錯誤訊息 |
| data | O | Object | 資料(無資料時回傳null) | 成功時才會有 |

#### data 欄位
M 為必要傳送欄位，O 為自由選擇傳送欄位。
| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| privateKey | M | String | 驗證OTP時所需的唯一鍵 |  |
| otpString | M | String | OTP |  |
| identityCode | M | String | 2碼數字大寫英文。識別號碼，用於讓用戶可判斷此 OTP 的簡易識別碼 |  |
| expiredSecond | M | Integer | OTP 有效的秒數 |  |
| expiredTime | M | String | OTP 無效(過期)的時間 | fmt:[yyyy/MM/dd hh:mm:ss] |
| returnMessage | M | String | 寄送結果訊息 |  |

---------------------  