保戶登入 - memberLogin
===
## 描述:
Step 1. 檢核欄位格式是否符合  
符合 → 則進行第2步  
不符合 → 則回傳[E0005]   
<br>
Step 2. 檢核驗證碼是否符合  
符合 → 則進行第3步   
不符合 → 則回傳[E1009]  
<br>
Step 3. 呼叫核心登入API  
密碼輸入錯誤第1次 → 則回傳[E1004]   
密碼輸入錯誤第2~4次 → 則回傳[E1005]  
密碼輸入錯誤第5次以上(含) → 則回傳[E1006] 並 帳號被鎖  

## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 昕力DEV | http://10.20.30.120:7001 |
| 測試環境 API 地址 | /chubb.apiTemplate/member/login?checkCode=1234 |
| 正式環境 API 地址 | N/A |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | checkCode=1234 |
| checkCode | 四碼驗證碼 |

### Body
```json=
{
    "account": "A12345678",
    "whisper": "asdW23456"
}
```

### Body 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| account | M | String | 會員帳號 | 身分證或居留證號，不可含特殊符號、長度不能超過10碼並符合身分證或新居留證號規範格式。 | 
| whisper | M | String | 會員密碼 | 輸入限制長度8-12位英數字混合字母，必須含至少一位大寫與一位小寫英文與阿拉伯數字。 |

## Response
```json=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data": {
        "loginToken": "eyJhbGciOiJIUzM4NCJ9.",
        "uuid":"asdfasdf-dsadasd-asdvsadf-adsfasdf",
        "passwordChangePrompt": {
              "overdue":true,
              "systemOverdue":90,
              "overdueDays":90,
              "acceptPassword":true
        }
        "user":{
            "cellphone":"0966****56",
            "name":"安*",
            "email":"ted***@chubb.com"
        },
        "readingRecord": true
    }
}
```
### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| status | M | String | HttpStatus狀態碼 | A0001成功<br>E0001:applicationToken不相符<br>E0002:帳號停用<br>E0003:資料驗證錯誤<br>E0008:密碼不符<br>E0009:帳號不存在<br>E0010:帳號已鎖定<br>E9999:未知錯誤 xxx<br>失敗請參考 [錯誤代碼表](/錯誤V1.0.md)|
| errCode | M | String | 後端exception代碼 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| errMsg | M | String | 後端exception代碼訊息資料 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| success | M | Boolean | 是否成功 | true/false |
| message | M | String | 訊息資料 | 成功/錯誤訊息 |
| data | O | Object | 資料(無資料時回傳null) | 成功時才會有 |

#### data 欄位
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| loginToken | O | String | 登入TOKEN | 登入成功才會回傳loginToken |
| uuid | M | String | 會員流流水號 |  |
| passwordChangePrompt | M | Object | 密碼提示訊息物件 |  |
| user | M | Object | 會員資訊 |  |
| readingRecord | M | Boolean | 是否勾選新閱讀聲明 | true/false<br>判斷上稿資料(TX_CMSNEW)與閱讀紀錄資料(TX_CMSNEW_READING_RECORE)是否需要勾選新閱讀聲明 |

#### passwordChangePrompt 欄位
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| overdue | M | Boolean | 是否提示變更密碼 | true/false |
| systemOverdue | M | Integer | 多少天未更新 |  |
| overdueDays | M | Integer | 系統預設多少天需修改 |  |
| acceptPassword | M | Boolean | 是否需強制變更密碼 | true/false |

#### user 欄位
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| cellphone | M | String | 隱碼電話號碼 |  |
| name | M | String | 隱碼姓名 |  |
| email | M | String | 隱碼電子信箱 |  |
 ------------------------------- 