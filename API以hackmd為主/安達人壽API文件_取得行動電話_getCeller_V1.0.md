取得行動電話 - getCell
===
## 描述:
若有取得資料，回傳給前端。

## 規格
| 項目                    | 說明                                         |
| ----------------------------- | ------------------------------------- |
| Method                  | POST
| Content-Type | application/json|
| 測試機                  |/member/getCeller
| 正式環境           |
| Header | authorization:Bearer [JWT]|

### Request 欄位
欄位名稱名稱 | 資料型別| 必填| 說明
 --------- | ------- | ------|--------
account | String | M  | 身分證字號
birthday | String | M  | 生日

### Request 範例
```gherkin=
{
    "account":"A123456789",
    "birthday": "1996/12/03"
}
```

## Response
上方為成功情況，下方為失敗情況
```json=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data": {
        "cellphone": "0987654321"
    },
}
```
```json=
{
    
    "status": "500",
    "errCode": "E9999",
    "errMsg": "系統繁忙，請稍後再操作",

}
```
### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。
<br>上方為成功情況，下方為失敗情況

| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| status | M | String | 狀態碼。<br>A0001 |
| success | M | Boolean | true |
| message | M | String | 成功 |
| data | O | Object | 資料(無資料時回傳null) |

| 欄位名稱名稱 | 資料型別|必要 |  說明|
| --------- | ------- | ------- | --------|
| status | String| M | 錯誤狀態碼(請見錯誤代碼表)|
| errCode | String | M |  錯誤代碼(請見錯誤代碼表)|
| errMsg | String | M |  錯誤訊息(請見錯誤代碼表)|
 ------------------------------- 

#### data 欄位
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| cellphone | M | String | 行動電話 |
 ------------------------------- 

