取得資訊公告 - getNews
===
## 描述:
依據種類(TX_CMSNEW.TYPE)取得上稿資訊，且將資訊依最新至最舊的排序(order by TX_CMSNEW.ID desc)傳到前端。

## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 昕力DEV | http://10.20.30.120:7001 |
| 測試環境 API 地址 | /chubb.apiTemplate/news/getNews |
| 正式環境 API 地址 | N/A |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | N/A |

### Body
```json=
{
    "types": [
        "01",
        "02"
    ],
    "uuid":"asdfasdf-dsadasd-asdvsadf-adsfasdf"
}
```

### Body 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。
| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| types | M | Object Array | 資訊種類<br>01.登入需知、<br>02.資訊公告、<br>03.保戶首頁最新公告、<br>04.閱讀聲明-個人資料保護法應告知事項、<br>05.閱讀聲明-網路保險服務聲明事項 | 可以一次給多筆 |
| uuid | O | String | 會員流流水號 |  |

## Response 傳送成功-登入需知、資訊公告
```json=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data": [
        {
            "id": 1,
            "type": "01",
            "title": "test title",
            "content": "this is a test",
            "startDate": "2022/01/19",
            "readingRecord": "N"
        },
        {
            "id": 99,
            "type": "02",
            "title": "test title2",
            "content": "this is a test2",
            "startDate": "2022/01/19",
            "readingRecord": "N"
        }
    ]
}
```
## Response 傳送成功-保戶首頁最新公告
```json=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data": [
        {
            "id": 199,
            "type": "03",
            "title": "test title",
            "content": "this is a test",
            "startDate": "2022/01/19",
            "readingRecord": "N"
        },
        {
            "id": 299,
            "type": "03",
            "title": "test title",
            "content": "this is a test",
            "startDate": "2022/01/19",
            "readingRecord": "Y"
        }
    ]
}
```
## Response 傳送成功-閱讀聲明-個人資料保護法應告知事項、閱讀聲明-網路保險服務聲明事項
```json=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data": [
        {
            "id": 399,
            "type": "04",
            "title": "test title",
            "content": "this is a test",
            "startDate": "2022/01/19",
            "readingRecord": "N"
        },
        {
            "id": 499,
            "type": "05",
            "title": "test title",
            "content": "this is a test",
            "startDate": "2022/01/19",
            "readingRecord": "N"
        }
    ]
}
```
## Response 傳送失敗
```json=
{
    "status": "500",
    "errCode": "E9999",
    "errMsg": "系統繁忙，請稍後再操作"
}
```
### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| status | M | String | HttpStatus狀態碼 | 成功會是A0001，失敗請參考 [錯誤代碼表](/錯誤V1.0.md)  |
| errCode | M | String | 後端exception代碼 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| errMsg | M | String | 後端exception代碼訊息資料 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| success | M | Boolean | 是否成功 | true/falus |
| message | M | String | 訊息資料 | success |
| data | O | Object | 資料(無資料時回傳null) | 成功時才會有 |

#### data 欄位
| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| id | M | Integer | 上稿資料流水號 |  |
| type | M | String | 資訊種類<br>01.登入需知、<br>02.資訊公告、<br>03.保戶首頁最新公告、<br>04.閱讀聲明-個人資料保護法應告知事項、<br>05.閱讀聲明-網路保險服務聲明事項 |  |
| title | M | String | 資訊標題 |  |
| content | M | String | 資訊內容 |  |
| startDate | M | String | 開始日期 | fmt:[yyyy/mm/dd] |
| readingRecord | M | String | 閱讀紀錄，Y/N | type=01,02,04,05時，預設都是給N<br> type=03時，需比對TX_CMSNEW_readingRecord是否閱讀過 |

 ------------------------------- 

