保戶網路會員登入之密碼提示訊息 - promptWhisper
===
## 描述:
1.呼叫核心保戶網路會員登入之密碼提示訊息API
<br>
2.完成後寄登入成功信件

## 規格
| 項目                    | 說明                                         |
| ----------------------------- | ------------------------------------- |
| Method                  | POST
| Content-Type | application/json|
| 測試機                  |/member/promptWhisper
| 正式環境           |
| Header | authorization:Bearer [JWT]|

### Request 欄位
欄位名稱名稱 | 資料型別| 必填| 說明
 --------- | ------- | ------|--------
loginToken | String | M  | 帳號登入TOKEN

### Request 範例
```gherkin=
{
    "loginToken"： "de83046b-7b7e-4550-9a6c-019ed503f200"
}
```

## Response
```json=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data": {
        "overdue":true,
        "systemOverdue":90,
        "overdueDays":90,
        "acceptPassword":true
    },
}
```
### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| status | M | Integer | A0001 代表成功；其他代號則代表失敗，可以透過msg獲得詳細的失敗原因<br>E0001:applicationToken不相符<br>E0003:資料驗證錯誤<br>E0007:登入失效<br>E9999:未知錯誤 XXXX |
| success | M | Boolean | true/false |
| message | M | String | 成功/錯誤訊息 |
| data | O | Object | 資料(無資料時回傳null) |

#### data 欄位
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| overdue | M | Boolean | 是否提示變更密碼 |
| systemOverdue | M | Integer | 多少天未更新 |
| overdueDays | M | Integer | 系統預設多少天需修改 |
| acceptPassword | M | Boolean | 是否需強制變更密碼 |
 ------------------------------- 

