契約變更資料牌卡 - contractChange
===
## 描述:
判斷保戶本地端db是否有資料，有則回傳沒有則呼叫核心並寫進本地端db

##備註:
現階段為傳統型，投資型部分的投資明細核心尚未提供，待提供後再更新

## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 測試環境 API 地址 ||
| 正式環境 API 地址 |  |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | N/A |

### Body
```json=
{
   "uuid":"f17e6096-8280-42c6-aea9-8c24e88fbcce",
   "policyCode":[
   "000002958758310",
   "000004084389318"
   ]
}
```

### Body 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| policyCode | M | Array | 保單號碼 ||
| uuid | M | String | 會員流水號 ||

## Response
### 成功
```json=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data":{
        "success":1,
        "statusChange":1,
        "detail":[
            {
            "policyCode":"000004084389318",
            "productName":"幸福寓守定期保險",
            "applyTime":"2021/11/11",
            "holderName":"王力宏",
            "insuredName":"Yumi",
            "serviceDesc":"受益人變更",
            "finishTime":"2021/11/11"
            },
            {
            "policyCode":"000002958758310",
            "productName":"幸福寓守定期保險",
            "applyTime":"2021/11/11",
            "holderName":"吳孟達",
            "insuredName":"周星馳",
            "serviceDesc":"解約",
            "finishTime":"2021/11/11"
            }
        ]
    }
}
```
### 失敗
```json=
{
    "status": "500",
    "errCode": "E9999",
    "errMsg": "系統繁忙，請稍後再操作",
}
```
### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。
#### 成功
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| status | M | String | 狀態碼。<br>A0001成功|
| success | M | Boolean | true/false |
| message | M | String | 成功/錯誤訊息 |
| data | O | Object | 資料(無資料時回傳null) |

#### 失敗
| 欄位名稱名稱 | 資料型別|必要 |  說明
| --------- | ------- | ------- | --------|
| status | String| M | 錯誤狀態碼(請見錯誤代碼表)|
| errCode | String | M |  錯誤代碼(請見錯誤代碼表)|
| errMsg | String | M |  錯誤訊息(請見錯誤代碼表)|

#### data 欄位
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| success | M | Integer |完成|
| statusChange | M | Integer |進行中|
| detail | O | Object |詳細資訊(無資料時回傳null)|

#### detail 欄位
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| policyCode | M | String |保單號碼|
| productName | M | String |商品名稱|
| applyTime | M | String |申請時間|
| holderName | M | String |保險人|
| insuredName | M | String |被保險人|
| serviceDesc | M | String |變更項目|
| finishTime | M | String |結案日|
 ------------------------------- 

