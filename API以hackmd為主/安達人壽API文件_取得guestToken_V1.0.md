取得guestToken- token
===
## 描述:
取得guest使用的jwt

## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 昕力DEV | http://10.20.30.120:7001 |
| 測試環境 API 地址 | /token |
| 正式環境 API 地址 | N/A |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | N/A |
| Body | N/A |

## Response 傳送成功
```json=
{
    "status": "A0001",
    "success": true,
    "message": "success",
    "data": {
        "token": "eyJhbGciOiJIUzM4NCJ9.eyJleHAiOjE2NDMyMDk4MTgsImp0aSI6IjI5ZmJiZjQ0LWZkZjMtNDFiOC04MTg0LTIxYmQwYmM1MjBmMyIsInN1YiI6Imd1ZXN0IiwiaWF0IjoxNjQyNTg0MDAwfQ.JhBIS7fuAg3Xi4seV2flewGY9260F6ulm6HY-KRNER83ikowJVoQoNHT9NZfGXXw",
        "expiration": 1642585800
    }
}
```

## Response 傳送失敗
```json=
{
    "status": "401",
    "errCode": "E0001",
    "errMsg": "無效token"
}
```

### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| status | M | String | HttpStatus狀態碼 | A0001成功<br>失敗請參考 [錯誤代碼表](/錯誤V1.0.md)|
| errCode | M | String | 後端exception代碼 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| errMsg | M | String | 後端exception代碼訊息資料 | 失敗時才會有，請見 [錯誤代碼表](/錯誤V1.0.md)  |
| success | M | Boolean | 是否成功 | true/false |
| message | M | String | 訊息資料 | 成功/錯誤訊息 |
| data | O | Object | 資料(無資料時回傳null) | 成功時才會有 |

#### data 欄位
| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| token | M | String | guest使用的jwt |  |
| expiration | M | Integer | token時效 |  |
 ------------------------------- 
