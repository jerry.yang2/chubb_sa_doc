理賠進度查詢牌卡 - policyClaimProgressRate
===
## 描述:
判斷保戶本地端db是否有資料，有則回傳沒有則呼叫核心並寫進本地端db


## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 測試環境 API 地址 |/policy/policyClaimProgressRate|
| 正式環境 API 地址 |  |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | N/A |

### Body
```json=
{
   "uuid":"f17e6096-8280-42c6-aea9-8c24e88fbcce",
   "policyCode":["xx1"]
}
```

### Body 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| policyCode | M | Array | 保單號碼 ||
| uuid | M | String | 會員流水號 ||

## Response
### 成功
```json=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data":{
        "success":1,
        "statusChange":0,
        "detail":[
            {
            "policyCode":"xx1",
            "productName":"幸福寓守定期保險",
            "acceptTime":"2021/11/11",
            "holderName":"王力宏",
            "insuredName":"Yumi",
            "auditConclusionDesc":"給付",
            "salesName":"龍五",
            "agentName":"台新銀行後甲",
            "agentCode":"NTS0035",
            "feeAmount":"200,000",
            "money":"新臺幣",
            "inspectStatusDesc": "照會中",
            "inspectFinishTime":"2022/01/28"
            }
        ]    
    }
}
```
### 失敗
```json=
{
    "status": "500",
    "errCode": "E9999",
    "errMsg": "系統繁忙，請稍後再操作",
}
```
### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。
#### 成功
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| status | M | String | 狀態碼。<br>A0001成功|
| success | M | Boolean | true/false |
| message | M | String | 成功/錯誤訊息 |
| data | O | Object | 資料(無資料時回傳null) |

#### 失敗
| 欄位名稱名稱 | 資料型別|必要 |  說明
| --------- | ------- | ------- | --------|
| status | String| M | 錯誤狀態碼(請見錯誤代碼表)|
| errCode | String | M |  錯誤代碼(請見錯誤代碼表)|
| errMsg | String | M |  錯誤訊息(請見錯誤代碼表)|

#### data 欄位
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| success | M | Integer |完成|
| statusChange | M | Integer |進行中|
| detail | O | Object |詳細資訊(無資料時回傳null)|

#### detail 欄位
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| policyCode | M | String |保單號碼|
| productName | M | String |商品名稱|
| acceptTime | M | String |受理日期|
| holderName | M | String |保險人|
| inspectFinishTime | M | String |照會期限|
| insuredName | M | String |被保險人|
| auditConclusionDesc | M | String |審核依據|
| inspectStatusDesc | M | String |照會狀態|
| salesName | M | String |業務員|
| agentName | M | String |通路名稱|
| agentCode | M | String |通路代碼|
| feeAmount | M | String |給付金額|
| money | M | String |保單幣別|
 ------------------------------- 

