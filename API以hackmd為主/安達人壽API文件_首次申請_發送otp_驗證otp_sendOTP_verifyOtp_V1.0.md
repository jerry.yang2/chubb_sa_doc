
OTP
===
取得OTP
===
描述: 
1.取得OTP驗證碼。
2.寄送OTP驗證碼。

規則:
提供給後端運用正確的Request來呼叫並操作取得驗證碼的程序。

其他:
相關程序的Request以及回傳內容說明詳見下方。

## 規格
| 項目                    | 說明                                         |
| ----------------------------- | ------------------------------------- |
| Method                  | POST    
| Content-Type | application/json|
| 測試機                  |/member/sendOTP
| 正式環境           | 
| Header | authorization:Bearer [JWT]

### Request 欄位
欄位名稱名稱 | 資料型別| 必填| 說明
 --------- | ------- | ------|--------
account | String | M/O  | 身分證字號。
cellphone | String | M/O  | 手機號碼。
email | String | M/O  | 電子信箱。
otpLength | Integer | O | OTP 字串長度。可選擇範圍為 6~12 碼。
expiredSecond | Integer | O  | OTP 存活時間。單位秒數，最低 60 秒，最大 900 秒。
validationErrorCount | Integer | O  | OTP 可驗證錯誤次數。最少 3 次，最大 10 次。
identityCodeLength | Integer | O  | OTP 識別碼長度。 可選擇範圍為 2~4 碼。
uuid | String | M/O  | 會員流水號
### Request 範例 -首次申請跟忘記密碼使用
```gherkin=
{
    "account": "A123456789",
    "cellphone": "0961000000",
    "email": "XXX@chubb.com",
    "otpLength": 6,    
    "expiredSecond": 300,
    "validationErrorCount": 3,
    "identityCodeLength": 2,
    "uuid":""
}
```
### Request 範例 -登入後變更密碼
```gherkin=
{
    "account": "",
    "cellphone": "",
    "email": "",
    "otpLength": 6,    
    "expiredSecond": 300,
    "validationErrorCount": 3,
    "identityCodeLength": 2
    "uuid":"asdfasdf-dsadasd-asdvsadf-adsfasdf"
}
```

## Response 欄位
M 為必要傳送欄位，O 為自由選擇傳送欄位。
<br>上方為成功情況，下方為失敗情況

| 欄位名稱名稱 | 資料型別|必要 |  說明 |
| --------- | ------- | ------- | --------|
| status | String| M | 狀態碼。<br>A0001|
| message | String | M |  成功|
| success | String | M |  true|
| data | Object Array | O | 資料列表(無資料時回傳null)|

| 欄位名稱名稱 | 資料型別|必要 |  說明
| --------- | ------- | ------- | --------|
| status | String| M | 錯誤狀態碼(請見錯誤代碼表)|
| errCode | String | M |  錯誤代碼(請見錯誤代碼表)|
| errMsg | String | M |  錯誤訊息(請見錯誤代碼表)|

備註:<br>
若errCode為E5002，則回傳的errMsg為"由於系統中斷，OTP發送失敗，請您選擇繼續申請或取消本次申請，謝謝。@@@由於系統中斷，稍後將回到首頁，需請您重新操作，造成您的不便，敬請見諒！"<br>
此時為發送otp失敗之情況，請前端若接到此response，幫忙記錄otp發送失敗次數。<br><br>
若失敗次數為1次，則錯誤訊息為errMsg的"@@@"前面的字串，即"由於系統中斷，OTP發送失敗，請您選擇繼續申請或取消本次申請，謝謝。"<br>
若失敗次數為>1次，則錯誤訊息為errMsg的"@@@"後面的字串，即"由於系統中斷，稍後將回到首頁，需請您重新操作，造成您的不便，敬請見諒！"<br>

#### data 欄位
| 欄位名稱 | 資料型別 | 必要 | 說明 |
| --------- | --- | ------- | ----- |
| privateKey | String | O | 驗證OTP時所需的唯一鍵|
| otpString | String | O | OTP |
| identityCode | String | O | 2碼數字大寫英文。識別號碼，用於讓用戶可判斷此 OTP 的簡易識別碼 |
| expiredSecond | Integer | O | OTP 有效的秒數 |
| expiredTime | String | O | OTP 無效(過期)的時間 |
| returnMessage | String | O | 寄送結果訊息 |


<br>上方為成功情況，下方為失敗情況
```gherkin=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data": {
        "privateKey": "UQyUkXZYvTEq4TSNFA32sP8KbHQ0Vjy8PAGE0SHI/3o6ZZSJapHjh",
        "otpString"： "737913",
        "identityCode"： "AA",
        "expiredSecond"： 300,
        "expiredTime"： "2022/01/10 17:22:43",
        "returnMessage": "OTP驗證碼已寄到您的手機號碼與電子信箱"
    }
}
```
```json=
{
    
    "status": "500",
    "errCode": "E9999",
    "errMsg": "系統繁忙，請稍後再操作",

}
```
---------------------
驗證OTP
===
###### tags: `MobileWeb`
描述: 
用於驗證OTP驗證碼是否正確。

規則:
提供給後端運用正確的Request來呼叫並操作驗證驗證碼的程序。

其他:
相關程序的Request以及回傳內容說明詳見下方。
## 規格
| 項目                    | 說明                                         |
| ----------------------------- | ------------------------------------- |
| Method                  | POST    
| Content-Type | application/json|
| 測試機    | /member/verifyOTP
| 測試機Gateway   | 
| 正式環境    | 
| Header | authorization:Bearer [JWT]|


### Request 欄位

欄位名稱名稱 | 資料型別| 必填| 說明
 --------- | ------- | ------|--------
privateKey | String | M| 驗證OTP時所需的唯一鍵
otpString | String | M | OTP

### Request 範例
```gherkin=
{
    "privateKey"： "UQyUkXZYvTEq4TSNFA32sP8KbHQ0Vjy8PAGE0SHI/3o6ZZSJapHjh",
    "otpString"： "737913"       
}
```

## Response 欄位

欄位名稱名稱 | 資料型別|  說明
 --------- | ------- | --------
status | String|  狀態碼。<br>A0001:成功
message | String|  資訊
success | Boolean|  是否成功
data | Object Array | 資料列表(無資料時回傳null)

| 欄位名稱名稱 | 資料型別|必要 |  說明
| --------- | ------- | ------- | --------|
| status | String| M | 錯誤狀態碼(請見錯誤代碼表)|
| errCode | String | M |  錯誤代碼(請見錯誤代碼表)|
| errMsg | String | M |  錯誤訊息(請見錯誤代碼表)|

##### 成功
```gherkin=

{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data": null    
}
```
```json=
{
    
    "status": "500",
    "errCode": "E9999",
    "errMsg": "系統繁忙，請稍後再操作",

}
```
---------------------
