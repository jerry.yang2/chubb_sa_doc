保單給付資料 - PolicyPaymentInformation
===
## 描述:
判斷保戶本地端db是否有資料，有則回傳沒有則呼叫核心並寫進本地端db


## Request
| 項目 | 說明 |
| ----------------------- | -------------------------------------------- |
| HTTP Method | POST |
| 測試環境 API 地址 |/policy/getPolicyPaymentInformation|
| 正式環境 API 地址 |  |
| Header | Content-Type:application/json; charset=UTF-8 |
| Header | authorization:Bearer [JWT] |
| Path Parameters | N/A |
| Query Parameters | N/A |

### Body
```json=
{
   "uuid":"c298db8c-18a3-4cb3-9e21-8a413859890b",
   "policyCode":"000002958758310"
}
```

### Body 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。

| 欄位名稱 | 必要 | 資料型別 | 說明 | 備註 |
| --------- | --- | ------- | ----- | ----- |
| policyCode | M | String | 保單號碼 ||
| uuid | M | String | 會員流水號 ||



## Response
### 成功
```json=
{
    "status": "A0001",
    "message": "success",
    "success": true,
    "data":[{
    "policyCode":"000002958758310", 
    "productName":"xxxxxxxx保險"
    "feeAmount":111.4, 
    "payWay":3, 
    "payWayDesc":"轉帳", 
    "bankName":"安達銀行", 
    "bankBranchName":"資訊分行", 
    "bankAccount":"1111111111111",
    "serviceId":202, 
    "serviceDesc":"解約",
    "moneyCode":"TWD", 
    "money":"新台幣",
    "newFinishedTime":"2014/05/11", 
    "liabilityStatusDesc":"有效",
    "insuredName":"安*",
    }]
}
```
### 失敗
```json=
{
    "status": "500",
    "errCode": "E9999",
    "errMsg": "系統繁忙，請稍後再操作",
}
```
### Response 說明
M 為必要傳送欄位，O 為自由選擇傳送欄位。
#### 成功
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| status | M | String | 狀態碼。<br>A0001成功|
| success | M | Boolean | true/false |
| message | M | String | 成功/錯誤訊息 |
| data | O | Object | 資料(無資料時回傳null) |

#### 失敗
| 欄位名稱名稱 | 資料型別|必要 |  說明
| --------- | ------- | ------- | --------|
| status | String| M | 錯誤狀態碼(請見錯誤代碼表)|
| errCode | String | M |  錯誤代碼(請見錯誤代碼表)|
| errMsg | String | M |  錯誤訊息(請見錯誤代碼表)|

#### data 欄位
| 欄位名稱 | 必要 | 資料型別 | 說明 |
| --------- | --- | ------- | ----- |
| policyCode | M | String |保單號碼|
| productName | M | String |商品名稱|
| feeAmount | M | Double |費用金額|
| payWay | M | Integer |付款方式代碼|
| payWayDesc | M | String |付款方式|
| bankName | M | String |付款銀行|
| bankBranchName | M | String |付款銀行-分行|
| bankAccount | M | String |匯款帳號(傳隱碼帳號給前端)|
| serviceId | M | Integer |給付類別代碼|
| serviceDesc | M | String |給付類別|
| moneyCode | M | String |幣別代碼|
| money | M | String |幣別|
| newFinishedTime | O | String |給付日期|
| liabilityStatusDesc | M | String |保單當前責任狀態|
| insuredName | M | String |被保險人姓名(傳隱碼姓名)|





 ------------------------------- 

