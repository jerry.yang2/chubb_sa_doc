# 安達保戶園地FS文件+API文件
[[_TOC_]]

## 共用資料
- [Http 狀態碼](/錯誤V1.0.md)  
- [取得guest token](/安達人壽API文件_取得guestToken_V1.0.md)   
- [FS-共用規則](/安達人壽保戶園地第一階段_共用規則_v1.0_20220111.docx)  

## G1
### FS
- [安達人壽需求規格書_保戶登入+天版+地板](/安達人壽需求規格書_登入_天版_地版_V2.0_20220117.docx)  
- [安達人壽需求規格書_首次申請](/安達人壽需求規格書_首次申請_V2.0_20220117.docx)  
- [安達人壽需求規格書_忘記密碼](/安達人壽需求規格書_忘記密碼_V2.0_20220117.docx)  

### API
#### 保戶登入  
- [產生驗證圖-getRandomCode](/安達人壽API文件_產生驗證圖-getRandomCode_V1.0.md)  
- [取得資訊公告-getNews](/安達人壽API文件_取得資訊公告-getNews_V1.0.md)  
- [登入-memberlogin](/安達人壽API文件_保戶登入-memberLogin_V1.0.md) 
#### 首次申請  
- [取得資訊公告-getNews](/安達人壽API文件_取得資訊公告-getNews_V1.0.md)  
- [核心身分驗證-checkMember](/安達人壽API文件_核心身分驗證-checkMember_V1.0.md)  
- [發送otp-sendOTP](/安達人壽API文件_發送otp-sendOTP_V1.0.md) 
- [核心首次申請-firstLogin](/安達人壽API文件_核心首次申請-firstLogin_V1.0.md)
#### 忘記密碼  
- [本地身分驗證-checkLocalMember](/安達人壽API文件_本地身分驗證-checkLocalMember_V1.0.md) 
- [發送otp-sendOTP](/安達人壽API文件_發送otp-sendOTP_V1.0.md) 
- [重新設定密碼-forgetWhisper](/安達人壽API文件_重新設定密碼-forgetWhisper_V1.0.md) 


## G2
### FS
- [安達人壽需求規格書_保戶首頁＋保單查詢] 
- [安達人壽需求規格書_變更密碼]
### API
#### 保戶首頁＋保單查詢  

#### 變更密碼  
